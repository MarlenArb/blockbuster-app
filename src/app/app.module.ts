import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent} from './header/header.component';
import { FooterComponent} from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ClienteService } from './cliente/cliente.service';
import { RouterModule, Routes} from '@angular/router';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegoService } from './juegos/juego.service';
import {HttpClientModule} from '@angular/common/http';
import { FormComponent } from './cliente/form.component';
import {FormsModule } from '@angular/forms';
import { TiendasComponent } from './tiendas/tiendas.component';
import { TiendaService } from './tiendas/tienda.service';
import { TformComponent } from './tiendas/tform.component';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData} from '@angular/common';
import { JformComponent } from './juegos/jform.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatMomentDateModule} from '@angular/material-moment-adapter';
import { PedidosComponent } from './pedidos/pedidos.component';
import { PedidoService } from './pedidos/pedido.service';
import { CompanyService } from './companies/company.service';
import { PformComponent } from './pedidos/pform.component';
import { CompaniesComponent } from './companies/companies.component';
import { CformComponent } from './companies/cform.component';
import { HomeComponent } from './home/home.component';
import { PipePagPipe } from './pipes/pipe-pag.pipe';
import { MatPaginatorModule } from '@angular/material/paginator';

registerLocaleData(localeEs, 'es');

const routes: Routes = [
  {path: ' ', redirectTo: 'home', pathMatch: 'full'}, //path vacio es nuestro home
  {path: 'directivas', component: DirectivaComponent},
  {path: 'cliente', component: ClienteComponent},
  {path: 'juego', component: JuegosComponent},
  {path: 'tienda', component: TiendasComponent},
  {path: 'pedido', component: PedidosComponent},
  {path: 'company', component: CompaniesComponent},
  {path: 'cliente/form', component: FormComponent},
  {path: 'cliente/form/:documento', component: FormComponent},
  {path: 'tienda/form', component: TformComponent},
  {path: 'tienda/form/:nombreTienda', component: TformComponent},
  {path: 'juego/form', component: JformComponent},
  {path: 'juego/form/:refJuego', component: JformComponent},
  {path: 'pedido/form', component: PformComponent},
  {path: 'pedido/form/:refPedido', component: PformComponent},
  {path: 'company/form', component: CformComponent},
  {path: 'company/form/:nombreCompany', component: CformComponent},
  {path: 'home', component: HomeComponent}

]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClienteComponent,
    JuegosComponent,
    FormComponent,
    TiendasComponent,
    TformComponent,
    JformComponent,
    PedidosComponent,
    PformComponent,
    CompaniesComponent,
    CformComponent,
    HomeComponent,
    PipePagPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule, MatDatepickerModule, MatMomentDateModule, MatPaginatorModule
  ],
  providers: [ClienteService, JuegoService, TiendaService, PedidoService, CompanyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
