import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import  Swal  from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';
@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  clientes: Cliente[] ;

  //Para paginar
  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,15,30,50]

  constructor(private clienteService: ClienteService ) { }

  ngOnInit(): void {

    this.clienteService.getClientes().subscribe (
      clientes => this.clientes = clientes
    );
  }

  removeCliente( cliente: Cliente) :void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Está seguro?',
      text: `¿Está seguro de que desea eliminar el cliente ${cliente.nombreCliente}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.clienteService.removeCliente(cliente.documento).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli => cli !== cliente)
        swalWithBootstrapButtons.fire(
          'Eliminado!',
          `El cliente ${cliente.nombreCliente} ha sido eliminado con éxito`,
          'success'
        )
        
      } 
    )
  }
    })
  }


  //Probando
  public Existe(documento): boolean{ 
    this.clienteService.getCliente(documento).subscribe(
      response => {
        this.clientes = this.clientes.filter(cli => cli.documento !== documento)
    return true;
  })
  return false;
  }


  //Para paginar
  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

}
