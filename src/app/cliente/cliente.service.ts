import { Injectable } from '@angular/core';
//import { CLIENTES} from './clientes.json'
import { Cliente } from './cliente.js';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import  { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';


@Injectable({providedIn: "root"})
export class ClienteService {
 
  private urlEndPoint: string = 'http://localhost:8090/api/cliente';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  public validaciones: string[];

  constructor(private http : HttpClient, private router: Router) { }

  //Listar clientes
  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.urlEndPoint).pipe(
      map(response => {
        let clientes = response as Cliente[];
        return clientes.map(cliente =>{
          cliente.fechaNac = formatDate(cliente.fechaNac, 'dd/MM/yyyy', 'es');
          return cliente;
        })
      })
    )
    };

    //Añadir cliente. Realmente mi función del back no retorna un cliente sino n string...pero bueno
    create(cliente: Cliente) : Observable<Cliente> {
      return this.http.post<Cliente>(this.urlEndPoint, cliente, {headers: this.httpHeaders}).pipe(
        catchError(e => {
          console.error(e.error.message);
          this.validaciones = e.error.valids;
          console.error(e.error.valids);
          Swal.fire('Error al crear cliente', e.error.message, 'error');
          return throwError(e)
        })
      );
    }

    //Encontrar un cliente por documento
    getCliente(documento): Observable<Cliente>{
      return this.http.get<Cliente>(`${this.urlEndPoint}/${documento}`).pipe(
        catchError(e => {
          this.router.navigate(['/cliente']);
          console.error(e.error.message);
          Swal.fire('Error al editar', e.error.message, 'error' );
          return throwError(e);
        })
      );
    }

    //Modificar un cliente
    modCliente(cliente: Cliente): Observable<Cliente>{
      return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.documento}`, cliente, {headers: this.httpHeaders}).pipe(
        catchError(e => {
          console.error(e.error.message);
          console.error(e.error.valids);
          this.validaciones = e.error.valids;
          Swal.fire('Error al editar cliente', e.error.message, 'error');
          return throwError(e)
        })
      );
    }

    //Eliminar cliente
    removeCliente(documento: String): Observable<Cliente>{
      return this.http.delete<Cliente>(`${this.urlEndPoint}/${documento}`,{headers: this.httpHeaders} ).pipe(
        catchError(e => {
          console.error(e.error.message);
          Swal.fire('Error al eliminar cliente', e.error.message, 'error');
          return throwError(e)
        })
      );
    }

}
