import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public titulo: string = "Crear Cliente";
  public cliente: Cliente = new Cliente;
  public nuevoCliente: boolean;
  public validadores: string[];

  constructor(public clienteService: ClienteService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  public changeNuevoCliente(): void{
    this.nuevoCliente = !this.nuevoCliente;

  }


  //Añadir Cliente
  public create(): void {
   
    this.clienteService.create(this.cliente).subscribe(
      cliente =>{
         this.router.navigate(['/cliente'])
        swal.fire('Nuevo cliente', `Cliente ${cliente.nombreCliente} creado con éxito!`, 'success');
      }
    )
  }

  //Buscar cliente por el documento
  cargarCliente(): void{
    this.activatedRoute.params.subscribe(params =>{
      let documento = params['documento']
      if(documento){

        this.changeNuevoCliente();
        this.clienteService.getCliente(documento).subscribe( (cliente) => this.cliente = cliente)
      }
    })
  }

  modCliente():void{
    this.clienteService.modCliente(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/cliente'])
        swal.fire('Cliente Actualizado', `Cliente ${cliente.nombreCliente} actualizado con éxito`, 'success')
      }
    )
  }

  volver():void{
    this.router.navigate(['/cliente']);
  }

}
