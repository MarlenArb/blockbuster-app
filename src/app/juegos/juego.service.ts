import { Injectable } from '@angular/core';
//import { JUEGOS } from './juegos.json'
import { Juego } from './juego.js';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import  { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';

@Injectable({providedIn: "root"})
export class JuegoService {

  private urlEndPoint: string = 'http://localhost:8090/api/juego'
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  public validaciones: string[];


  constructor(private http : HttpClient,  private router: Router) { }

  getJuegos(): Observable<Juego[]> {
    
    return this.http.get<Juego[]>(this.urlEndPoint).pipe(
      map(response => {
        let juegos = response as Juego[];
        return juegos.map(juego =>{
          juego.fechaLanzamiento = formatDate(juego.fechaLanzamiento, 'dd/MM/yyyy', 'es');
          return juego;
        })
      })
    )
    
  }


   //Añadir juego
   addJuego(juego: Juego) : Observable<Juego> {
    return this.http.post<Juego>(this.urlEndPoint, juego, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        this.validaciones = e.error.valids;
        console.error(e.error.valids);
        Swal.fire('Error al crear juego', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

  //Encontrar un juego por referencia
  getJuego(refJuego): Observable<Juego>{
    return this.http.get<Juego>(`${this.urlEndPoint}/${refJuego}`).pipe(
      catchError(e => {
        this.router.navigate(['/juego']);
        console.error(e.error.message);
        Swal.fire('Error al editar', e.error.message, 'error' );
        return throwError(e);
      })
    );
  }

  //Modificar un juego
  modJuego(juego: Juego): Observable<Juego>{
    return this.http.put<Juego>(`${this.urlEndPoint}/${juego.refJuego}`, juego, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        console.error(e.error.valids);
        this.validaciones = e.error.valids;
        Swal.fire('Error al editar juego', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

  //Eliminar juego
  removeJuego(refJuego: String): Observable<Juego>{
    return this.http.delete<Juego>(`${this.urlEndPoint}/${refJuego}`,{headers: this.httpHeaders} ).pipe(
      catchError(e => {
        console.error(e.error.message);
        Swal.fire('Error al eliminar juego', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

}


