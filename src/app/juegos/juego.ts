import { Pedido } from '../pedidos/pedido';

export class Juego {

  titulo: string;
  fechaLanzamiento: string;
  pegi: number;
  refJuego: string;
  categoria: string;
  //refPedido: string;
  companies: string[];
  pedido: Pedido;
}
