import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Juego } from './juego';
import { JuegoService } from './juego.service';

@Component({
  selector: 'app-jform',
  templateUrl: './jform.component.html',
  styleUrls: ['./jform.component.css']
})
export class JformComponent implements OnInit {

  public titulo: string = "Crear Juego";
  public juego: Juego = new Juego;
  public nuevoJuego: boolean;
  public validadores: string[];
  public categorias: string[] = ['ESTRATEGIA', 'ACCION', 'AVENTURA', 'AZAR'];


  constructor(public juegoService: JuegoService,
              private router: Router,
               private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarJuego();
  }

  public changeNuevoJuego(): void{
    this.nuevoJuego = !this.nuevoJuego;

  }


  //Añadir Juego nuevo
  public addJuego(): void {
   
    this.juegoService.addJuego(this.juego).subscribe(
      juego =>{
         this.router.navigate(['/juego'])
        swal.fire('Nuevo juego', `Juego ${juego.titulo} creado con éxito!`, 'success');
      }
    )
  }

  //Buscar juego por la referencia
  cargarJuego(): void{
    this.activatedRoute.params.subscribe(params =>{
      let refJuego = params['refJuego']
      if(refJuego){

        this.changeNuevoJuego();
        this.juegoService.getJuego(refJuego).subscribe( (juego) => this.juego = juego)
      }
    })
  }

  //Modificar juego existente
  modJuego():void{
    this.juegoService.modJuego(this.juego).subscribe(
      juego => {
        this.router.navigate(['/juego'])
        swal.fire('Juego Actualizado', `Juego ${juego.titulo} actualizado con éxito`, 'success')
      }
    )
  }

  volver():void{
    this.router.navigate(['/juego']);
  }

}
