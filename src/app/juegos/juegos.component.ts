import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {

  juegos: Juego[];

  constructor(private juegoService: JuegoService) { }

  ngOnInit(): void {

    this.juegoService.getJuegos().subscribe (
      juegos => this.juegos = juegos
    );
  }

  removeJuego( juego: Juego) :void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Está seguro?',
      text: `¿Está seguro de que desea eliminar el juego ${juego.titulo}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.juegoService.removeJuego(juego.refJuego).subscribe(
          response => {
            this.juegos = this.juegos.filter(ju => ju !== juego)
        swalWithBootstrapButtons.fire(
          'Eliminado!',
          `El juego ${juego.titulo} ha sido eliminado con éxito`,
          'success'
        )
        
      } 
    )
  }
    })
  }

}
