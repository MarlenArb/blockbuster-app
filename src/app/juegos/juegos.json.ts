import { Juego } from './juego';

export const JUEGOS: Juego[] =[
  {titulo: "Juego1", fechaLanzamiento: "01/01/2010", pegi: 18, refJuego: "1J", categoria: "ACCION", companies:["Company1", "Company2"]},
  {titulo: "Juego2", fechaLanzamiento: "01/01/2010", pegi: 18, refJuego: "2B", categoria: "ACCION", companies:["Company1", "Company2"]},
  {titulo: "Juego3", fechaLanzamiento: "01/01/2010", pegi: 18, refJuego: "3A", categoria: "ACCION", companies:["Company1", "Company2"]}
];