import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import  { catchError } from 'rxjs/operators'
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Tienda } from './tienda';

@Injectable({providedIn: 'root'})
export class TiendaService {


  private urlEndPoint: string = 'http://localhost:8090/api/tienda';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  public validaciones: string[];

  constructor(private http : HttpClient, private router: Router) { }

  //Listar Tiendas
  getTiendas(): Observable<Tienda[]> {
    return this.http.get<Tienda[]>(this.urlEndPoint);
    };

    //Añadir tienda
    addTienda(tienda: Tienda) : Observable<Tienda> {
      return this.http.post<Tienda>(this.urlEndPoint, tienda, {headers: this.httpHeaders}).pipe(
        catchError(e => {
          console.error(e.error.message);
          this.validaciones = e.error.valids;
          console.error(e.error.valids);
          Swal.fire('Error al crear tienda', e.error.message, 'error');
          return throwError(e)
        })
      );
    }

    //Encontrar un tienda por su nombre
    getTienda(nombreTienda): Observable<Tienda>{
      return this.http.get<Tienda>(`${this.urlEndPoint}/${nombreTienda}`).pipe(
        catchError(e => {
          this.router.navigate(['/tienda']);
          console.error(e.error.message);
          Swal.fire('Error al editar', e.error.message, 'error' );
          return throwError(e);
        })
      );
    }

    //Modificar una tienda
    modTienda(tienda: Tienda): Observable<Tienda>{
      return this.http.put<Tienda>(`${this.urlEndPoint}/${tienda.nombreTienda}`, tienda, {headers: this.httpHeaders}).pipe(
        catchError(e => {
          console.error(e.error.message);
          console.error(e.error.valids);
          this.validaciones = e.error.valids;
          Swal.fire('Error al editar tienda', e.error.message, 'error');
          return throwError(e)
        })
      );
    }

    //Eliminar tienda
    removeTienda(nombreTienda: String): Observable<Tienda>{
      return this.http.delete<Tienda>(`${this.urlEndPoint}/${nombreTienda}`,{headers: this.httpHeaders} ).pipe(
        catchError(e => {
          console.error(e.error.message);
          Swal.fire('Error al eliminar tienda', e.error.message, 'error');
          return throwError(e)
        })
      );
    }
}
