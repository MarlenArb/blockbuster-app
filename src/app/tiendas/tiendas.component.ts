import { Component, OnInit } from '@angular/core';
import { TiendaService } from './tienda.service';
import { Tienda } from './tienda';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.css']
})
export class TiendasComponent implements OnInit {

  tiendas: Tienda[];

  constructor(private tiendaService: TiendaService) { }

  ngOnInit(): void {

    this.tiendaService.getTiendas().subscribe (
      tiendas => this.tiendas = tiendas
    );
  }
  
  //Eliminar Tienda
  removeTienda( tienda: Tienda) :void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Está seguro?',
      text: `¿Está seguro de que desea eliminar la tienda ${tienda.nombreTienda}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.tiendaService.removeTienda(tienda.nombreTienda).subscribe(
          response => {
            this.tiendas = this.tiendas.filter(cli => cli !== tienda)
        swalWithBootstrapButtons.fire(
          'Eliminada!',
          `La tienda ${tienda.nombreTienda} ha sido eliminada con éxito`,
          'success'
        )
        
      } 
    )
  }
    })
  }

}

