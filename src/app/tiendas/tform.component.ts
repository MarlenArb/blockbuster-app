import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Tienda } from './tienda';
import { TiendaService } from './tienda.service';


@Component({
  selector: 'app-tform',
  templateUrl: './tform.component.html',
  styleUrls: ['./tform.component.css']
})
export class TformComponent implements OnInit {

  public titulo: string = "Crear Tienda";
  public tienda: Tienda = new Tienda;
  public nuevaTienda: boolean;
  public validadores: string[];

  constructor(public tiendaService: TiendaService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarTienda();
  }

  public changeNuevaTienda(): void{
    this.nuevaTienda = !this.nuevaTienda;

  }


  //Añadir Cliente
  public addTienda(): void {
   
    this.tiendaService.addTienda(this.tienda).subscribe(
      tienda =>{
         this.router.navigate(['/tienda'])
        swal.fire('Nueva tienda', `Tienda ${tienda.nombreTienda} creada con éxito!`, 'success');
      }
    )
  }

  //Buscar cliente por el documento
  cargarTienda(): void{
    this.activatedRoute.params.subscribe(params =>{
      let nombreTienda = params['nombreTienda']
      if(nombreTienda){

        this.changeNuevaTienda();
        this.tiendaService.getTienda(nombreTienda).subscribe( (tienda) => this.tienda = tienda)
      }
    })
  }

  modTienda():void{
    this.tiendaService.modTienda(this.tienda).subscribe(
      tienda => {
        this.router.navigate(['/tienda'])
        swal.fire('Tienda Actualizada', `Tienda ${tienda.nombreTienda} actualizada con éxito`, 'success')
      }
    )
  }

  volver():void{
    this.router.navigate(['/tienda']);
  }

}
