import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Pedido } from './pedido';
import { PedidoService } from './pedido.service';
import { Juego } from '../juegos/juego';
import { JuegoService } from '../juegos/juego.service';

@Component({
  selector: 'app-pform',
  templateUrl: './pform.component.html',
  styleUrls: ['./pform.component.css']
})
export class PformComponent implements OnInit {

  titulo: string = "Crear Pedido";
  juegos: Juego[];
  public pedido: Pedido = new Pedido;
  public nuevoPedido: boolean;
  public validadores: string[];
  public estados: string[] = ['ALQUILADO', 'VENDIDO', 'EN STOCK'];


  constructor(public pedidoService: PedidoService,
              public juegoService: JuegoService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.cargarPedido();

    this.juegoService.getJuegos().subscribe(
      juegos => this.juegos = juegos);

  }

  public changeNuevoPedido(): void{
    this.nuevoPedido = !this.nuevoPedido;

  }


  //Añadir Pedido
  public addPedido(): void {
    this.pedidoService.addPedido(this.pedido).subscribe(
      pedido =>{
         this.router.navigate(['/pedido'])
        swal.fire('Nuevo pedido', `Pedido ${pedido.refPedido} creado con éxito!`, 'success');
      }
    )
  }

  //Buscar pedido por la referencia
  cargarPedido(): void{
    this.activatedRoute.params.subscribe(params =>{
      let refPedido = params['refPedido']
      if(refPedido){

        this.changeNuevoPedido();
        this.pedidoService.getPedido(refPedido).subscribe( (pedido) => this.pedido = pedido)
      }
    })
  }

  modPedido():void{
    this.pedidoService.modPedido(this.pedido).subscribe(
      pedido => {
        this.router.navigate(['/pedido'])
        swal.fire('Pedido Actualizado', `Pedido ${pedido.refPedido} actualizado con éxito`, 'success')
      }
    )
  }

  volver():void{
    this.router.navigate(['/pedido']);
  }


}
