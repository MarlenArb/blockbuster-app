import { Juego } from '../juegos/juego';

export class Pedido {

  estado: String ;
	refPedido: String ;
	documentoCliente: String ; //client who makes the order
	nombreTienda: String ; //the store where I make the order
	juegos: Juego[];
}
