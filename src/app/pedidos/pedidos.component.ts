import { Component, OnInit } from '@angular/core';
import  Swal  from 'sweetalert2';
import { Pedido } from './pedido';
import { PedidoService } from './pedido.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  pedidos: Pedido[];

  constructor(private pedidoService: PedidoService) { }

  ngOnInit(): void {

    this.pedidoService.getPedidos().subscribe (
      pedidos => this.pedidos = pedidos
    );
  }

  removePedido( pedido: Pedido) :void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Está seguro?',
      text: `¿Está seguro de que desea eliminar el pedido ${pedido.refPedido}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.pedidoService.removePedido(pedido.refPedido).subscribe(
          response => {
            this.pedidos = this.pedidos.filter(ped => ped !== pedido)
        swalWithBootstrapButtons.fire(
          'Eliminado!',
          `El pedido ${pedido.refPedido} ha sido eliminado con éxito`,
          'success'
        )
        
      } 
    )
  }
    })
  }

}
