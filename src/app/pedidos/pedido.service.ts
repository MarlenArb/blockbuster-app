import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import  { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Pedido } from './pedido';
import { Juego } from '../juegos/juego';


@Injectable({providedIn: 'root'})
export class PedidoService {

  private urlEndPoint: string = 'http://localhost:8090/api/pedido';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  public validaciones: string[];
 
  //juegos: Observable<Juego[]>;

  constructor(private http : HttpClient, private router: Router) { }

  //Listar Pedidos
  getPedidos(): Observable<Pedido[]> {
    return this.http.get<Pedido[]>(this.urlEndPoint);
    };


  //Añadir pedido
  addPedido(pedido: Pedido) : Observable<Pedido> {
    return this.http.post<Pedido>(this.urlEndPoint, pedido, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        this.validaciones = e.error.valids;
        console.error(e.error.valids);
        Swal.fire('Error al crear pedido', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

  //Encontrar un pedido por su referencia
  getPedido(refPedido): Observable<Pedido>{
    return this.http.get<Pedido>(`${this.urlEndPoint}/${refPedido}`).pipe(
      catchError(e => {
        this.router.navigate(['/pedido']);
        console.error(e.error.message);
        Swal.fire('Error al editar', e.error.message, 'error' );
        return throwError(e);
      })
    );
  }

  //Modificar un pedido existente
  modPedido(pedido: Pedido): Observable<Pedido>{
    return this.http.put<Pedido>(`${this.urlEndPoint}/${pedido.refPedido}`, pedido, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        console.error(e.error.valids);
        this.validaciones = e.error.valids;
        Swal.fire('Error al editar pedido', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

  //Eliminar pedido
  removePedido(refPedido: String): Observable<Pedido>{
    return this.http.delete<Pedido>(`${this.urlEndPoint}/${refPedido}`,{headers: this.httpHeaders} ).pipe(
      catchError(e => {
        console.error(e.error.message);
        Swal.fire('Error al eliminar pedido', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

}
