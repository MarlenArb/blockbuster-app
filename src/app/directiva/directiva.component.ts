import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html'
})
export class DirectivaComponent{

 listaEntidades: string[] = ['Cliente', 'Tienda', 'Pedido', 'Company', 'Juego'];
 habilitar: boolean = true;
 
 public estado ( hab: boolean){ 
   return hab==true?'Ocultar' : 'Mostrar';
    
  }

  constructor() { }

 setHabilitar(): void {
   this.habilitar = (this.habilitar == true)? false : true;
 }

}
