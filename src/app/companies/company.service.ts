import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import  { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Company } from './company';

@Injectable({providedIn: 'root'})
export class CompanyService {

  private urlEndPoint: string = 'http://localhost:8090/api/company';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  public validaciones: string[];
 
  constructor(private http : HttpClient, private router: Router) { }

  //Listar Companies
  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.urlEndPoint);
    };


  //Añadir company
  addCompany(company: Company) : Observable<Company> {
    return this.http.post<Company>(this.urlEndPoint, company, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        this.validaciones = e.error.valids;
        console.error(e.error.valids);
        Swal.fire('Error al crear compañia', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

  //Encontrar una company por su nombre
  getCompany(nombreCompany): Observable<Company>{
    return this.http.get<Company>(`${this.urlEndPoint}/${nombreCompany}`).pipe(
      catchError(e => {
        this.router.navigate(['/company']);
        console.error(e.error.message);
        Swal.fire('Error al editar', e.error.message, 'error' );
        return throwError(e);
      })
    );
  }

  //Modificar una company existente
  modCompany(company: Company): Observable<Company>{
    return this.http.put<Company>(`${this.urlEndPoint}/${company.nombreCompany}`, company, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        console.error(e.error.valids);
        this.validaciones = e.error.valids;
        Swal.fire('Error al editar compañia', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

  //Eliminar company
  removeCompany(nombreCompany: String): Observable<Company>{
    return this.http.delete<Company>(`${this.urlEndPoint}/${nombreCompany}`,{headers: this.httpHeaders} ).pipe(
      catchError(e => {
        console.error(e.error.message);
        Swal.fire('Error al eliminar compañia', e.error.message, 'error');
        return throwError(e)
      })
    );
  }

}

