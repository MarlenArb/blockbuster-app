import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Juego } from '../juegos/juego';
import { Company } from './company';
import { CompanyService } from './company.service';
import { JuegoService } from '../juegos/juego.service';

@Component({
  selector: 'app-cform',
  templateUrl: './cform.component.html',
  styleUrls: ['./cform.component.css']
})
export class CformComponent implements OnInit {

  titulo: string = "Crear Company";
  juegos: Juego[];
  public company: Company = new Company;
  public nuevaCompany: boolean;
  public validadores: string[];

  constructor(public companyService: CompanyService,
              public juegoService: JuegoService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.cargarCompany();

    this.juegoService.getJuegos().subscribe(
      juegos => this.juegos = juegos);
  }
  public changeNuevaCompany(): void{
    this.nuevaCompany = !this.nuevaCompany;

  }


  //Añadir Pedido
  public addCompany(): void {
    this.companyService.addCompany(this.company).subscribe(
      company =>{
         this.router.navigate(['/company'])
        swal.fire('Nueva compañia', `Compañia ${company.nombreCompany} creada con éxito!`, 'success');
      }
    )
  }

  //Buscar company por nombre
  cargarCompany(): void{
    this.activatedRoute.params.subscribe(params =>{
      let nombreCompany = params['nombreCompany']
      if(nombreCompany){

        this.changeNuevaCompany();
        this.companyService.getCompany(nombreCompany).subscribe( (company) => this.company = company)
      }
    })
  }

  //Modificar company existente
  modCompany():void{
    this.companyService.modCompany(this.company).subscribe(
      company => {
        this.router.navigate(['/company'])
        swal.fire('Compañia Actualizado', `Compañia ${company.nombreCompany} actualizado con éxito`, 'success')
      }
    )
  }

  volver():void{
    this.router.navigate(['/company']);
  }

}
