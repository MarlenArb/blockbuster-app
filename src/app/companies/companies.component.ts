import { Component, OnInit } from '@angular/core';
import { CompanyService } from './company.service';
import { Company } from './company';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  companies: Company[];

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {

    this.companyService.getCompanies().subscribe(
      companies => this.companies = companies
    )
  }

  removeCompany( company: Company) :void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Está seguro?',
      text: `¿Está seguro de que desea eliminar la compañía ${company.nombreCompany} y sus respectivos juegos`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.companyService.removeCompany(company.nombreCompany).subscribe(
          response => {
            this.companies = this.companies.filter(com => com !== company)
        swalWithBootstrapButtons.fire(
          'Eliminado!',
          `La compañia ${company.nombreCompany} ha sido eliminado con éxito`,
          'success'
        )
        
      } 
    )
  }
    })
  }


}
